/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function dataValidate()
{
    var name = document.getElementById("txtName");
    var contact = document.getElementById("txtContact");
    var email = document.getElementById("txtEmail");
    var details = document.getElementById("txtDetails");
	var pax = document.getElementById("txtPax");
	
    if(name.value=="" && contact.value=="" && email.value=="" && details.value=="" && pax.value=="")
    {
        alert("Please fill in all fields!");
    }
    else
    {
	if(name.value=="")
        {
            alert("Please fill in a name!");
        }
        else if(contact.value=="")
        {
            alert("Please fill in your contact!");
        }
        else if(email.value=="")
        {
            alert("Please fill in your email address!");
        }
        else if(details.value=="")
        {
            alert("Please fill in your details!");
        }
		else if(pax.value=="")
        {
            alert("Please fill in your number of pax!");
        }
        else
        {
            document.form1.action = "sendmail.php";
            document.form1.submit();
        }
    }
}

function valEmail()
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.getElementById("txtEmail").value;
    var status = new Boolean();
    if(reg.test(address) == false) 
    {
      alert('Invalid Email Address');
      document.getElementById("txtEmail").value="";
      document.getElementById("txtEmail").style.background="#ccc";
      status = false;
    }
    else
    {
        document.getElementById("txtEmail").style.background="#fff";
        status = true;
    }
    return status;
}

function valContact()
{
	var contactReg = /^([\+?0-9]*)$/;
    var contact = document.getElementById("txtContact").value;
    var status = new Boolean();
    if(contactReg.test(contact)==false) 
    {
        alert("Invalid contact number!");
        document.getElementById("txtContact").value="";
        document.getElementById("txtContact").style.background="#ccc";
        status = false;
    }
    else
    {
        document.getElementById("txtContact").style.background="#fff";
        status = true;
    }
    return status;
}

function valPax()
{
    var paxReg =  /^([0-9]*[\+?]*)$/;
    var pax = document.getElementById("txtPax").value;
    var status = new Boolean();
    if(paxReg.test(pax)==false) 
    {
        alert("Invalid pax number, number only!");
        document.getElementById("txtPax").value="";
        document.getElementById("txtPax").style.background="#ccc";
        status = false;
    }
    else
    {
        document.getElementById("txtPax").style.background="#fff";
        status = true;
    }
    return status;
}


//function valName()
//{
//    var name = document.getElementById("txtName").value;
//    var nameReg = /^([A-Za-z\s])+$/;
//    var status = new Boolean();
//        if(nameReg.test(name.trim())==false)
//        {
//            alert("Invalid name, don't put any number or symbol!");
//            document.getElementById("txtName").value="";
//            document.getElementById("txtName").style.background="#ff7e0e";
//            status = false;
//        }
//        else
//        {
//            status = true;
//            document.getElementById("txtName").style.background="#414141";
//        }
//    return status;
//}

//function valPlate()
//{
//  var plateReg = /^(A-Z)+(0-9)$/;
//	var plate = document.getElementById("txtPlate").value;
//  var status = new Boolean();
//        if(plateReg.test(plate) == false)
//        {
//            alert("Invalid plate number!");
//            document.getElementById("txtPlate").value="";
//            document.getElementById("txtPlate").style.background="#ff7e0e";
//            status = false;
//        }
//        else
//        {
//            status = true;
//            document.getElementById("txtPlate").style.background="#414141";
//        }
//    return status;
//}


